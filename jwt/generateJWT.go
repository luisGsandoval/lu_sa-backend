package jwt

import (
	"time"

	"bitbucket.org/luisGsandoval/lu_sa-backend/config"
	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
	"github.com/dgrijalva/jwt-go"
)

// GenerateJWT generates a jwt token with a payload assigned to it
func GenerateJWT(t models.User) (string, error) {

	JWTSECRET := config.Get("JWTSECRET")
	secret := []byte(JWTSECRET)

	payload := jwt.MapClaims{
		"id":       t.ID,
		"email":    t.Email,
		"name":     t.Name,
		"lastname": t.Lastname,

		"exp": time.Now().Add(24 * time.Hour).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	tokenStr, err := token.SignedString(secret)

	if err != nil {
		return "", err
	}

	return tokenStr, nil
}
