package models

// User es el struct que me permite definir el modelo para usuarios
type User struct {
	ID       int    `json:"id" sql:"id"`
	Email    string `json:"email" sql:"email"`
	Password string `json:"password,omitempty" sql:"password"`

	Name      string `json:"name,omitempty" sql:"name"`
	Lastname  string `json:"lastname,omitempty" slq:"lastname"`
	Telephone string `json:"telephone" sql:"telephone"`
	Address   string `json:"address" sql:"address"`
}
