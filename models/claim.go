package models

import (
	"github.com/dgrijalva/jwt-go"
)

// Claim is the structure needed to process the jwt
type Claim struct {
	Email string `json:"email"`
	ID    int    `json:"_id,omitempty"`
	jwt.StandardClaims
}
