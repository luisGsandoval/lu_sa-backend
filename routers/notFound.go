package routers

import (
	"html/template"
	"net/http"
)

// Tpl is what allows us to use templates
var Tpl *template.Template

// NotFound is the handler that sends the notFound html
func NotFound(w http.ResponseWriter, r *http.Request) {
	Tpl.ExecuteTemplate(w, "notFound.html", nil)
}
