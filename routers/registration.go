package routers

import (
	"encoding/json"
	"net/http"
	"strings"

	"bitbucket.org/luisGsandoval/lu_sa-backend/db"
	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
)

// Registration is the function that registers new users
func Registration(w http.ResponseWriter, r *http.Request) {

	var t models.User
	var data models.User
	err := json.NewDecoder(r.Body).Decode(&t)

	// Error checking
	if err != nil {
		http.Error(w, "Error in the data: \n"+err.Error(), http.StatusBadRequest)
		return
	}

	if len(t.Email) == 0 {
		http.Error(w, "Email required", http.StatusBadRequest)
		return
	}

	if len(t.Password) < 6 {
		http.Error(w, "Password must have more than 6 characters", http.StatusBadRequest)
		return
	}

	// sanitazing data
	t.Email = strings.TrimSpace(strings.ToLower(t.Email))

	// checking before saving
	data, err = db.UserExistanceCheck(t.Email)

	if err != nil {
		http.Error(w, "Error at checking the user existance. "+err.Error(), http.StatusBadRequest)
		return
	}

	if data.Email == t.Email {
		http.Error(w, "Error: user already exists", http.StatusBadRequest)
		return
	}

	// Saving data
	err = db.UserResgistration(t)

	// Posible responses
	if err != nil {
		http.Error(w, "An error was encountered when saving the user. "+err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusCreated)

}
