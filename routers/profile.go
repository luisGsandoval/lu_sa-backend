package routers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/luisGsandoval/lu_sa-backend/db"
	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
)

// FindProfile allows us to extract values from the profile
func FindProfile(w http.ResponseWriter, r *http.Request) {

	// ID := r.URL.Query().Get("id")

	// if len(ID) == 0 {
	// 	http.Error(w, "Profile not identified", http.StatusBadRequest)
	// 	return
	// }
	profile, err := db.FetchProfileData(UserID)

	if err != nil {
		http.Error(w, "Couldn't find the profile"+err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(profile)

}

// EditProfile allows us to modify values from the profile
func EditProfile(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Content-Type", "application/json")

	var t models.User

	err := json.NewDecoder(r.Body).Decode(&t)

	if err != nil {
		http.Error(w, "Couldn't decode body: "+err.Error(), http.StatusBadRequest)
		return
	}

	err = db.EditProfileData(t, UserID)

	if err != nil {
		http.Error(w, "Couldn't update the profile: "+err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusCreated)

}
