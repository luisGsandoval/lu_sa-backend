package routers

import (
	"errors"

	"bitbucket.org/luisGsandoval/lu_sa-backend/config"
	"bitbucket.org/luisGsandoval/lu_sa-backend/db"
	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
	jwt "github.com/dgrijalva/jwt-go"
)

// Email is the user's email returned from the request to the data base
var Email string

// UserID is the user id returned from the request to the data base
var UserID int

// ProcessToken extracts the values in the token
func ProcessToken(tk string) (*models.Claim, error) {
	secret := []byte(config.Get("JWTSECRET"))
	claims := &models.Claim{}

	jwtkn, err := jwt.ParseWithClaims(tk, claims, func(token *jwt.Token) (interface{}, error) {
		return secret, nil
	})

	if err != nil {
		return claims, err
	}

	if !jwtkn.Valid {
		return claims, errors.New("Invalid token: " + err.Error())
	}

	usr, err := db.UserExistanceCheck(claims.Email)

	if err != nil {
		return claims, err
	}

	Email = usr.Email
	UserID = usr.ID

	return claims, nil

}
