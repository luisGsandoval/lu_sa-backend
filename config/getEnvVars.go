package config

import (
	"os"
	"reflect"
)

// C stands for config where the env variables are going to b stored
type C struct {
	PORT          string `envName:"PORT" default:"8088"`
	MYSQLCXSTRING string `envName:"MYSQLCXSTRING" default:"b4669c487ae1b8:56a77156@tcp(us-cdbr-east-02.cleardb.com:3306)/heroku_363a35d9c1f06f3"`
	JWTSECRET     string `envName:"JWTSECRET" default:"unbelieveable-how-this-thing-works"`
}

// Get environment variables or the default value
func Get(fld string) string {
	confg := reflect.TypeOf(C{})
	f, _ := confg.FieldByName(fld)
	v, _ := f.Tag.Lookup("envName")
	dv, _ := f.Tag.Lookup("default")
	envValue := os.Getenv(v)

	if envValue == "" {
		return dv
	}

	return envValue

}
