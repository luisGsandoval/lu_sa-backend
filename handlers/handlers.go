package handlers

import (
	"log"
	"net/http"

	"bitbucket.org/luisGsandoval/lu_sa-backend/config"
	"bitbucket.org/luisGsandoval/lu_sa-backend/middleware"
	"bitbucket.org/luisGsandoval/lu_sa-backend/routers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

// HandleFuncs es el manejadr de rutas
func HandleFuncs() {
	router := mux.NewRouter()

	// All routers can be found here
	// Public
	router.HandleFunc("/api/registration", middleware.DBCheck(routers.Registration)).Methods("POST")
	router.HandleFunc("/api/login", middleware.DBCheck(routers.Login)).Methods("POST")

	// private
	router.HandleFunc("/api/profile", middleware.DBCheck(middleware.ValidateJWT(routers.FindProfile))).Methods("GET")
	router.HandleFunc("/api/edit-profile", middleware.DBCheck(middleware.ValidateJWT(routers.EditProfile))).Methods("POST")

	// CORS config
	handler := cors.AllowAll().Handler(router)

	log.Println("Server running on port: ", config.Get("PORT"))
	log.Fatal(http.ListenAndServe(":"+config.Get("PORT"), handler))

}
