package db

import (
	"log"

	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
)

// FetchProfileData is a function that brings information about a specific profile
func FetchProfileData(ID int) (models.User, error) {

	var profile models.User

	db, err := ConnectDB()

	if err != nil {
		log.Panic(err.Error())
		return profile, err
	}

	defer db.Close()

	if err != nil {
		log.Panic(err.Error())
		return profile, err

	}

	err = db.QueryRow(`SELECT 
											id, 
											COALESCE(name, '') as name, 
											COALESCE(lastname, '') as lastname, 
											COALESCE(email, '') as email, 
											COALESCE(address, '') as address, 
											COALESCE(telephone, '') as telephone
											
										FROM users 
										WHERE id=? 
										LIMIT 1`,
		ID).Scan(&profile.ID, &profile.Name, &profile.Lastname, &profile.Email, &profile.Address, &profile.Telephone)

	if err != nil {
		log.Panic(err.Error())
		return profile, err

	}

	return profile, nil

}
