package db

import (
	"log"

	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
)

// EditProfileData is a function that allows the user to modify information about a specific profile
func EditProfileData(usr models.User, uID int) error {

	db, err := ConnectDB()

	if err != nil {
		log.Panic(err.Error())
		return err
	}

	defer db.Close()
	updUsr, err := db.Prepare(`UPDATE users 

															SET 
																name=?,
																lastname=?,
																email=?,
																address=?,
																telephone=?  

															WHERE id=?`)
	if err != nil {
		log.Panic(err.Error())
		return err
	}

	defer updUsr.Close()

	_, err = updUsr.Exec(usr.Name, usr.Lastname, usr.Email, usr.Address, usr.Telephone, uID)

	if err != nil {
		log.Panic(err.Error())
		return err

	}

	return nil

}
