package db

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	"bitbucket.org/luisGsandoval/lu_sa-backend/config"
	// adviced with nalnk idenifiers
	_ "github.com/go-sql-driver/mysql"
)

// ConnectDB is where the db is innitiated
func ConnectDB() (*sql.DB, error) {

	var conectxString = config.Get("MYSQLCXSTRING")
	db, err := sql.Open("mysql", conectxString)
	if err != nil {
		log.Panic(err.Error())
		return db, errors.New("MySQl connection not established" + err.Error())
	}

	return db, nil

}

// StartingProjFromDB this function was created to help start a new DB with less hassle
func StartingProjFromDB() error {
	db, err := ConnectDB()
	if err != nil {
		return err
	}

	defer db.Close()

	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS users (
		id INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		name VARCHAR(30),
		lastname VARCHAR(30),
		email VARCHAR(50) NOT NULL,
		address  VARCHAR(100),
		telephone  INT(30),
		password VARCHAR(1000) NOT NULL
	);`)

	if err != nil {
		log.Panic(err.Error())
		fmt.Println("Table users creation check process failed")
		return err
	}

	fmt.Println("Table users creation check process succesfull")

	return nil
}

// ConnectionCheck is a test done to the database to make sure it's working as expected
func ConnectionCheck() error {

	db, err := ConnectDB()

	if err != nil {
		return err
	}

	err = db.Ping()

	if err != nil {
		return err
	}

	return nil

}
