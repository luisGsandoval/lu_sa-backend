package db

import (
	"errors"
	"log"

	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
)

// UserResgistration this function saves new users in the Database
func UserResgistration(u models.User) error {

	db, err := ConnectDB()
	if err != nil {
		return err
	}
	defer db.Close()

	u.Password, _ = EncrypPassword(u.Password)

	insForm, err := db.Prepare("INSERT INTO users(email, password) VALUES(?,?)")
	if err != nil {
		log.Panic(err.Error())
		return errors.New("insert not executed: " + err.Error())
	}

	if _, err = insForm.Exec(u.Email, u.Password); err != nil {
		return err
	}

	return nil

}
