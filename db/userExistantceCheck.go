package db

import (
	"log"

	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
)

// UserExistanceCheck checks if the user already exists to avoid duplicity
func UserExistanceCheck(email string) (models.User, error) {

	usr := models.User{}
	db, err := ConnectDB()
	if err != nil {
		log.Panic(err.Error())
		return usr, err
	}
	defer db.Close()

	// err = db.
	// 	QueryRow("SELECT id, email, password FROM users WHERE email=? LIMIT 1", email).
	// 	Scan(&usr.ID, &usr.Email, &usr.Password)

	res, err := db.Query("SELECT id, email, password FROM users WHERE email=? LIMIT 1", email)

	if err != nil {
		return usr, err
	}

	for res.Next() {

		err = res.Scan(&usr.ID, &usr.Email, &usr.Password)
		if err != nil {
			log.Panic("error: " + err.Error())
			return usr, err
		}

	}

	return usr, nil

}
