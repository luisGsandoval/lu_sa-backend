package db

import (
	"errors"

	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
	"golang.org/x/crypto/bcrypt"
)

// ValidatePasswords functionalty
func ValidatePasswords(email, password string) (models.User, error) {

	user, err := UserExistanceCheck(email)

	if err != nil {
		return user, errors.New("Email wasn't found")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))

	if err != nil {
		return user, errors.New("passwords don't match: " + err.Error())
	}

	return user, nil

}
