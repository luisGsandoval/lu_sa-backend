package middleware

import (
	"net/http"

	"bitbucket.org/luisGsandoval/lu_sa-backend/db"
)

// DBCheck is a function that allows me to check the database status
func DBCheck(next http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		if err := db.ConnectionCheck(); err != nil {
			http.Error(w, "DB Connection lost."+err.Error(), http.StatusInternalServerError)
			return
		}

		next.ServeHTTP(w, r)

	}

}
