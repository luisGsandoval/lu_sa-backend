package main

import (
	"html/template"
	"log"

	"bitbucket.org/luisGsandoval/lu_sa-backend/db"
	"bitbucket.org/luisGsandoval/lu_sa-backend/handlers"
	"bitbucket.org/luisGsandoval/lu_sa-backend/routers"
)

// Tpl for golang templates
var Tpl *template.Template

func init() {
	routers.Tpl = template.Must(template.ParseGlob("templates/*.html"))
}

func main() {

	// start DB
	if err := db.StartingProjFromDB(); err != nil {
		log.Fatal("No table was created MySQL")
		return
	}
	if err := db.ConnectionCheck(); err != nil {
		log.Fatal("No connection to MySQL")
		return
	}
	log.Println("MySQL connected succesfully")

	handlers.HandleFuncs()
}
